﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MovingObject {

    public int wallDamage = 1;
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public int pointsPerSuperSoda = 100;
    public float restartLevelDelay = 1f;
    public Text foodText;
    public Text wallWait;
    public Text scoreText;
    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip eatSound1;
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;
    public AudioClip gameOverSound;
    public GameObject playerWall;

    private Animator animator;
    private int food;
    private bool canPlaceWall;
    private int wallTimer;
    private string direction;
    private Rigidbody2D rb2d;
    private int score;

	// Use this for initialization
	protected override void Start () {
        animator = GetComponent<Animator>();

        food = GameManager.instance.playerFoodPoints;
        score = GameManager.instance.playerScore;

        foodText.text = "Food " + food;

        rb2d = GetComponent<Rigidbody2D>();

        wallTimer = 20;
        canPlaceWall = false;

        base.Start();
		
	}

    private void OnDisable()
    {
        GameManager.instance.playerFoodPoints = food;
        GameManager.instance.playerScore = score;
    }
	
	// Update is called once per frame
	void Update () {

        scoreText.text = "Score: " + score;

        if (!GameManager.instance.playersTurn) return;

        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");

        if (horizontal != 0)
            vertical = 0;

        if (horizontal != 0 || vertical != 0)
            AttemptMove<Wall>(horizontal, vertical);

        if (horizontal == 1 && vertical == 0)
            direction = "Right";

        if (horizontal == -1 && vertical == 0)
            direction = "Left";

        if (horizontal == 0 && vertical == 1)
            direction = "Up";

        if (horizontal == 0 && vertical == -1)
            direction = "Down";

        if (wallTimer > 0)
        {
            wallWait.text = "Next Wall: " + wallTimer;
        }

        else
        {
            wallTimer = 0;
            wallWait.text = "Next Wall: 0";
            canPlaceWall = true;
        }     

        if (Input.GetKey("space") && canPlaceWall)
        {
            //print("I placed a wall in " + direction + " direction.");

            switch (direction)
            {
                case "Up":
                    Instantiate(playerWall, new Vector3(rb2d.position.x, rb2d.position.y + 1, 0f), Quaternion.identity);
                    break;

                case "Down":
                    Instantiate(playerWall, new Vector3(rb2d.position.x, rb2d.position.y - 1, 0f), Quaternion.identity);
                    break;

                case "Left":
                    Instantiate(playerWall, new Vector3(rb2d.position.x - 1, rb2d.position.y, 0f), Quaternion.identity);
                    break;

                case "Right":
                    Instantiate(playerWall, new Vector3(rb2d.position.x + 1, rb2d.position.y, 0f), Quaternion.identity);
                    break;
            }

            score += 25;
            canPlaceWall = false;
            wallTimer = 20;
        }

		
	}

    protected override void AttemptMove <T> (int xDir, int yDir)
    {
        food--;
        foodText.text = "Food: " + food;
        score += 5;
        wallTimer -= 1;

        base.AttemptMove<T>(xDir, yDir);

        RaycastHit2D hit;
        if (Move(xDir, yDir, out hit))
        {
            SoundManager.instance.RandomizeSfx(moveSound1, moveSound2);
        }

        CheckIfGameOver();

        GameManager.instance.playersTurn = false;
    }

    private void OnTriggerEnter2D (Collider2D other)
    {
        if (other.tag == "Exit")
        {
            Invoke("Restart", restartLevelDelay);
            enabled = false;
        }
        else if (other.tag == "Food")
        {   
            food += pointsPerFood;
            score += pointsPerFood * 5;
            foodText.text = "+" + pointsPerFood + " Food: " + food;
            SoundManager.instance.RandomizeSfx(eatSound1, eatSound2);
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Soda")
        {
            food += pointsPerSoda;
            score += pointsPerSoda * 5;
            foodText.text = "+" + pointsPerSoda + " Food: " + food;
            SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "SuperSoda")
        {
            food += pointsPerSuperSoda;
            score += pointsPerSuperSoda * 5;
            foodText.text = "+" + pointsPerSuperSoda + " Food: " + food;
            SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);
            other.gameObject.SetActive(false);
        }
    }

    protected override void OnCantMove <T> (T component)
    {
        Wall hitWall = component as Wall;
        hitWall.DamageWall(wallDamage);
        animator.SetTrigger("playerChop");
        score += 10;
    }

    private void Restart()
    {
        SceneManager.LoadScene(0);
    }

    public void LoseFood (int loss)
    {
        animator.SetTrigger("playerHit");
        food -= loss;
        score -= 50;
        CheckIfGameOver();
    }
    private void CheckIfGameOver()
    {
        if (food <= 0)
        {
            SoundManager.instance.PlaySingle(gameOverSound);
            SoundManager.instance.musicSource.Stop();
            GameManager.instance.GameOver();
        }
    }
}