﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;

    private float offsetX;
    private float offsetY;

    // Use this for initialization
    void Start() {
        offsetX = transform.position.x - player.transform.position.x;
        offsetY = transform.position.y - player.transform.position.y;
    }

    // Update is called once per frame
    void LateUpdate() {
        float playerX = player.transform.position.x + offsetX;
        float playerY = player.transform.position.y + offsetY;
        if (playerX > -0.5f && playerX < 51.5f)
        {
            transform.position = new Vector3(playerX, transform.position.y, transform.position.z);
        }

        if (playerY > 3.2f && playerY < 34.9f)
        {
            transform.position = new Vector3(transform.position.x, playerY, transform.position.z);
        }
    }
}
